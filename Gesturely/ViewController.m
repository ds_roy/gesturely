//
//  ViewController.m
//  Gesturely
//
//  Created by Jason on 04/17/13.
//  Copyright (c) 2013 Me. All rights reserved.
//

#import "ViewController.h"
#import "CollectionViewCell.h"

// don't use #define for things like this. you could easily confuse ACTUAL preprocessor macros..
static NSString *const CELL_ID = @"cell_id";

@interface ViewController () <UICollectionViewDelegateFlowLayout>
@property (nonatomic, assign) CGSize cellSize;
@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:CELL_ID];

    UICollectionViewFlowLayout *collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
    self.cellSize = CGSizeMake(300, 150);

    self.collectionView.collectionViewLayout = collectionViewLayout;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        CGFloat width = arc4random() % 300 + 100;
        CGFloat height = arc4random() % 150 + 50;

        if (!width)
            width = 100;
        if (!height)
            height = 500;

        self.cellSize = CGSizeMake(width, height);

        UICollectionViewFlowLayout *collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];

        [self.collectionView setCollectionViewLayout:collectionViewLayout animated:YES];
    }
}

#pragma mark -
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 100;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = self.cellSize;
    if (indexPath.row % 3 == 0)
    {
        size.width *= 1.5;
        size.height *= 1.5;
    }
    return size;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELL_ID forIndexPath:indexPath];

    cell.backgroundColor = indexPath.row % 2 == 0 ? [UIColor redColor] : [UIColor greenColor];
    [collectionView.panGestureRecognizer requireGestureRecognizerToFail:cell.pullDownGestureRecognizer];


    return cell;
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"SELECTED INDEX %d", indexPath.row);
}


#pragma mark -
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    for (CollectionViewCell *cell in self.collectionView.visibleCells)
    {
        [cell hideContentView];
    }
}


@end