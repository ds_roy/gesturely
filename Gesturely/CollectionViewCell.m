//
// Created by jason on 4/17/13.
//
//

#import <UIKit/UIGestureRecognizerSubclass.h>
#import <CoreGraphics/CoreGraphics.h>
#import "CollectionViewCell.h"


@interface CollectionViewCell () <UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIView *coverView;
@end

@implementation CollectionViewCell

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }

    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.clipsToBounds = YES;
        _coverView = [[UIView alloc] initWithFrame:self.bounds];
        _coverView.backgroundColor = [UIColor blackColor];
        _coverView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:_coverView];

        _pullDownGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
        _pullDownGestureRecognizer.minimumNumberOfTouches = 2;
        _pullDownGestureRecognizer.delegate = self;
        [self addGestureRecognizer:_pullDownGestureRecognizer];

    }

    return self;
}


- (void)handleGesture:(UIPanGestureRecognizer *)gesture
{
    if (gesture == self.pullDownGestureRecognizer && gesture.numberOfTouches == 2 && gesture.state == UIGestureRecognizerStateChanged)
    {
        CGPoint location = [gesture locationInView:self.contentView];

        CGRect frame = self.coverView.frame;
        frame.origin.y = location.y;
        if (frame.origin.y <= 0)
            frame.origin.y = 0;
        self.coverView.frame = frame;

        CGFloat percent = (self.contentView.bounds.size.height - location.y) / self.contentView.bounds.size.height;
        self.coverView.alpha = percent;
    }
    else if (gesture == self.pullDownGestureRecognizer && gesture.state == UIGestureRecognizerStateEnded)
    {
        if (self.coverView.frame.origin.y < self.bounds.size.height/2)
            [self hideContentView];
        else
            [self showContentView];
    }
}



#pragma mark -
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];

    [self.pullDownGestureRecognizer touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];

    [self.pullDownGestureRecognizer touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];

    [self.pullDownGestureRecognizer touchesEnded:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];

    [self.pullDownGestureRecognizer touchesCancelled:touches withEvent:event];
}


#pragma mark -
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}


#pragma mark -
- (void)hideContentView
{
    [UIView animateWithDuration:.25 animations:^
    {
        self.coverView.alpha = 1;
        self.coverView.frame = self.bounds;
    } completion:nil];
}

- (void)showContentView
{
    [UIView animateWithDuration:.25 animations:^
    {
        self.coverView.alpha = 0;
        CGRect frame = self.coverView.frame;
        frame.origin.y = self.bounds.size.height;
        self.coverView.frame = frame;
    } completion:nil];
}

@end